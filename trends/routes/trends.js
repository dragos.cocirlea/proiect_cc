const express = require('express');
const router = express.Router();
const Trend = require('../models/Trend');

router.post('/', async (req, res) => {
    const haircutData = req.body;

    Trend.find({name: haircutData.name})
    .then(trends => {
        if (trends.len > 0) {
            res.status(409);
        } else {
            Trend.create({
                name: haircutData.name,
                description: haircutData.description,
                hits: 0
            }).
            then(trend => {
                res.json(trend);
            })
            .catch(err => {
                console.log(err);
                res.status(500);
            })
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500);
    })
})

router.get('/', async (req, res) => {
    Trend.find()
    .then(trends => {
        res.json(trends.sort((a, b) => b.hits - a.hits));
    })
    .catch(err => {
        console.log(err);
        res.status(500);
    })
})

router.get('/:haircutID', async (req, res) => {
    Trend.findById(req.params.haircutID)
    .then(trend => {
        res.json(trend);
    })
    .catch(err => {
        console.log(err);
        res.status(500);
    })
})

router.patch('/:haircutID', async (req, res) => {
    Trend.findByIdAndUpdate(
        req.params.haircutID,
        {$inc: {hits: 1}},
        {new: true}
    )
    .then(trend => {
        res.json(trend);
    })
    .catch(err => {
        console.log(err);
        res.status(500);
    })
})

module.exports = router;